# Project: Epic Wordle By EMOF  

## 1. Preface  

### 1.1 Context  
**Epic Wordle By EMOF** est un jeu qui consiste à deviner un mot de cinq à huit lettres en six tentatives ou moins. Ce jeu a été réalisé dans le cadre d'un projet de programmation en 3ème année de Licence Informatique au Centre d'Etudes et de Recherches Internationales de l'Université d'Avignon avec la surveillance des enseignants: Noe Cecilllon, Jarod Duret.  
  
### 1.2 Environment  
  
**Epic Wordle By EMOF** est un jeu codé essentiellement en Java (Standard Edition Version 10, https://docs.oracle.com/javase/10/docs/api/index.html?overview-summary.html). 
Dans un souci de portabilité, la convention de nommage en Java a été respectée (https://www.geeksforgeeks.org/java-naming-conventions/).

**Epic Wordle By EMOF** comporte une section qui fait appel à un traitement automatique de la langue (TAL) qui fait référence à la représentation vectorielle de mots dans un espace continu à l'aide d'un modèle pré-entrainé et dans l'objectif de trouver la similarité de mots. Ce traitement est effectif sur un serveur distant HTTP. Cette section a été codée en Python v3 (voir le Chapitre X. Partie de Olivier ).

### 1.2 Versionning 

**Epic Wordle By EMOF** est accessible sous licence GPL v3 sur Gitlab.

### 1.4 Licence

**Wordle** est une marque déposée, propriété du New York Times (2022).
**Epic Wordle By EMOF** est un jeu sous la licence publique générale GNU (GPL version 3, https://en.wikipedia.org/wiki/GNU_General_Public_License).


### 1.5 Game Developers

**Olivier Fabre** (Development Manager):  
Coordinateur entre l'équipe du projet.  
Responsable de la réalisation du fichier README.  
Responsable de la réalisation du rapport de projet.  
Responsable de la conception un système d’indices basé sur la similarité entre mots (Section Word Embedding, connexion Word2vec - application Java).  

**Francois Demogue** (Development): 
Responsable Architecture du jeu (classes, méthodes, attributs, liens). 
Responsable Developpement du coeur du jeu qui permets de jouer une partie selon les règles classiques du jeu wordle.  

**Matheo Fauchez** (Development): 
Responsable du versionning du jeu (gitlab). 
Responsable du developpement de l'affichage des élements du jeu.  
Responsable du developpement de la sauvegarde des informations relatives à la partie.  

**Enzo Llorca** (Development): 
Responsable du développement du portail général au lancement et des menus de l’application. 

## X. Partie de François

néant


## X. Partie de Mathéo

néant

## X. Partie d'Enzo

néant

## X. Partie de Olivier

néant